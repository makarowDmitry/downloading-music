import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Класс осуществляет поиск ссылок на музыку и картинки, а также поиск названий песен
 */
public class AttributesSite {
    public static ArrayList<String> nameMusic = new ArrayList();

    /**
     * Метод произвоит чтение html с сайта и находит в нём ссылки на медиафайлы и их имена
     *
     * @param Url - ссылка на сайт
     * @return - возвращает ArrayList с ссылками для скачивания медиафайлов
     */
    public static ArrayList<String> searchUrls(String Url) {
        ArrayList<String> listLinks = new ArrayList();
        try {
            Document document = Jsoup.connect(Url).get();
            Elements elementsLinks = document.select("ul.unstyled.songs").select("li.item");
            for (Element element : elementsLinks) {
                listLinks.add(element.select("li.play").attr("data-url"));
                nameMusic.add(element.select("a").attr("title"));
                listLinks.add("https://muzika.vip" + element.select("img").attr("data-src"));
            }
            deletePartsTitle();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listLinks;
    }

    /**
     * Метод удаляет не нужные части в строках
     */
    private static void deletePartsTitle() {
        String delete = "Песня исполнителя ";
        for (int i = 0; i < nameMusic.size(); i++) {
            nameMusic.set(i, nameMusic.get(i).replace(delete, ""));
        }
    }
}
