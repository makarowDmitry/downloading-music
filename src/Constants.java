public interface Constants {
    String PATH_TO_MUSIC = "src\\Music\\";
    String PATH_TO_IMAGE = "src\\Image\\image";
    String MP3 = ".mp3";
    String JPG = ".jpg";
    int NUMBER_OF_FIND = 3;
    String URL = "https://muzika.vip/";
}
