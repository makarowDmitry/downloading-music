import java.util.ArrayList;

public class ThreadPlayer extends Thread implements Constants {
    private static ArrayList namesOfMusic = new ArrayList();

    /**
     * @param namesOfMusic - ArrayList с названиями песен из папки Music
     */
    ThreadPlayer(ArrayList namesOfMusic) {
        this.namesOfMusic = namesOfMusic;
    }

    /**
     * Поток осуществляющий запуск метода musicPlayer из класса MusicPlayer
     */
    public void run() {
        try {
            while (MusicPlayer.stopCount) {
                System.out.println("Играет: " + namesOfMusic.get(MusicPlayer.countOfPlayer));
                MusicPlayer.musicPlayer();
                if (MusicPlayer.stopCount) {
                    MusicPlayer.countOfPlayer++;
                    MusicPlayer.timeOfPlayer = 0;
                    if (MusicPlayer.countOfPlayer == NUMBER_OF_FIND) {
                        MusicPlayer.countOfPlayer = 0;
                    }
                }
            }
        } catch (IndexOutOfBoundsException e) {
            System.out.printf("Нет песни с номером " + (MusicPlayer.countOfPlayer + 1));
        }
    }
}
