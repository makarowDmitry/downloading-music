
import java.io.*;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.ArrayList;

/**
 * Класс осуществляет скачивание медиафайлов
 */
public class Download implements Constants {

    /**
     * Метод запускает скачивание файла и даёт ему имя
     *
     * @param links - массив ссылок для скачивания
     */
    public static void startDownload(ArrayList<String> links, int count) {
        for (; count < NUMBER_OF_FIND * 2; count += 2) {
            try {
                if (count % 2 == 0 || count == 0) {
                    downloadUsingNIO(links.get(count), PATH_TO_MUSIC + AttributesSite.nameMusic.get(count) + MP3);
                } else {
                    downloadUsingNIO(links.get(count), PATH_TO_IMAGE + count + JPG);
                }
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }

    /**
     * Метод осуществляет закачку файла по ссылке
     *
     * @param strUrl - ссылка скачивания
     * @param file   - путь куда сохранять и имя файла
     * @throws IOException - срабатывает если отсутствует интернет, не правильная ссылка
     */
    private static void downloadUsingNIO(String strUrl, String file) throws IOException{
        FileOutputStream stream;
        URL url = new URL(strUrl);
        try (ReadableByteChannel byteChannel = Channels.newChannel(url.openStream())){
            stream = new FileOutputStream(file);
            stream.getChannel().transferFrom(byteChannel, 0, Long.MAX_VALUE);
            stream.close();
       }
    }
}
