
public class ThreadImage extends Thread {
    private String url;

    /**
     * @param url - ссылка
     */
    ThreadImage(String url) {
        this.url = url;
    }

    /**
     * Поток осуществляет запуск метода startDownload из класса Download для скачивания изображений
     */
    @Override
    public void run() {
        Download.startDownload(AttributesSite.searchUrls(url),1);
    }
}
