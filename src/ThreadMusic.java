

public class ThreadMusic extends Thread {

    private String url;

    /**
     * @param url - ссылка
     */
    ThreadMusic(String url) {
        this.url = url;
    }

    /**
     * Поток осуществляет запуск метода startDownload из класса Download для скачивания аудиофайлов
     */
    public void run() {
        Download.startDownload(AttributesSite.searchUrls(url),0);
    }
}