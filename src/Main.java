
/**
 * Класс осуществляет запуск потоков и считывает из файла ссылку на сайт
 *
 * @author Макаров Д.А, Волбенко А.К.
 */
public class Main implements Constants {

    public static void main(String[] args) {
        ThreadMusic threadMusic = new ThreadMusic(URL);
        ThreadImage threadImage = new ThreadImage(URL);
        threadMusic.start();
        threadImage.start();
        try {
            threadMusic.join();
            threadImage.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        MusicPlayer.player();
    }
}