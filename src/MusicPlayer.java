import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.advanced.AdvancedPlayer;
import javazoom.jl.player.advanced.PlaybackEvent;
import javazoom.jl.player.advanced.PlaybackListener;

import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Scanner;

public class MusicPlayer implements Constants {

    private static AdvancedPlayer player;
    private static ThreadPlayer threadPlayer;
    private static ArrayList namesOfMusic = new ArrayList();
    private static Scanner sc = new Scanner(System.in);
    public static int countOfPlayer = 0;
    public static boolean stopCount = true;
    public static double timeOfPlayer = 0;

    public static void startPlayer() {
        System.out.println("Старт плеера");
        musicPath();
        enterface();
    }

    /**
     * Метод используется для записи названия файлов в ArrayList из srs/Music
     */
    private static void musicPath() {
        try {
            File dir = new File("src\\Music");
            for (File item : dir.listFiles()) {
                namesOfMusic.add(item.getName());
            }
        } catch (NullPointerException e) {
            System.out.println("\n__\n Нет папки Music\n___\n");
            System.exit(0);
        }
    }

    /**
     * Метод осуществляет выбор действий, который должен выполнить плеер
     */
    private static void enterface() {
        int sw = 4;
        while (true) {
            switch (sw) {
                case 0:
                    close();
                case 1:
                    play();
                    break;
                case 2:
                    stop();
                    break;
                case 3:
                    chooseMusic();
                    break;
                case 4:
                    buttens();
                    break;
                case 5:
                    if (threadPlayer == null) {
                        System.out.println("ня");
                    }
            }
            sw = sc.nextInt();
        }
    }

    private static void play() {
        stopCount = true;
        if (threadPlayer == null || threadPlayer.getState() == Thread.State.TERMINATED) {
            startThreadPlayer();
        } else {
            System.out.println("Уже играет: " + namesOfMusic.get(countOfPlayer));
        }
    }

    private static void stop() {
        stopCount = false;
        if (threadPlayer == null || threadPlayer.getState() == Thread.State.TERMINATED) {
            System.out.println("Нет запущенной песни");
        } else {
            player.stop();
            System.out.println("Пауза");
        }
    }

    private static void chooseMusic() {
        for (int count = 0; count < namesOfMusic.size(); count++) {
            System.out.println(count + 1 + ". " + namesOfMusic.get(count));
        }

        System.out.println("Введите номер песни(для отмены введите 0)");
        countOfPlayer = sc.nextInt();

        if (countOfPlayer == 0) {
            System.out.println("Отмена");
        } else {
            countOfPlayer--;
            if (threadPlayer != null || threadPlayer.getState() != Thread.State.TERMINATED) {
                stopCount = false;
                player.stop();
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        stopCount = true;
        timeOfPlayer = 0;
        startThreadPlayer();
    }

    private static void buttens() {
        System.out.println("1 - Старт проигрывания\n2 - Пауза\n3 - Выбрать песню\n4 - Показать клавиши\n0 - Закрыть плеер");
    }


    private static void close() {
        System.out.println("Закрытие плеера");
        System.exit(0);
    }

    /**
     * Метод осуществляет объявление и запуск потока ThreadPlayer
     */
    private static void startThreadPlayer() {
        threadPlayer = new ThreadPlayer(namesOfMusic);
        threadPlayer.start();
    }

    /**
     * Метод осуществляет воспроизведение, паузу mp3 плеера с помощью библеотеки JLayer
     */
    public static void musicPlayer() {
        double countOfSecond = 23.5;
        try (InputStream threadIn = new FileInputStream(PATH_TO_MUSIC + namesOfMusic.get(countOfPlayer))) {
            player = new AdvancedPlayer(threadIn);
            player.setPlayBackListener(new PlaybackListener() {
                public void playbackFinished(PlaybackEvent event) {
                    double timeOfPlayerTest = event.getFrame();
                    timeOfPlayerTest = timeOfPlayerTest / 1000 * countOfSecond;
                    timeOfPlayer = timeOfPlayer + timeOfPlayerTest;
                    timeOfPlayer = new BigDecimal(timeOfPlayer).setScale(0, RoundingMode.HALF_UP).doubleValue();
                    timeOfPlayer = timeOfPlayer + event.getFrame();
                }
            });
            player.play((int) timeOfPlayer, Integer.MAX_VALUE);
        } catch (FileNotFoundException ignore) {
        } catch (IOException | JavaLayerException e) {
            e.printStackTrace();
        }
    }

}
